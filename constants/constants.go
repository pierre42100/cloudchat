package constants

// MinNameLength .
const MinNameLength int = 3

// MinPasswordLength .
const MinPasswordLength int = 3

// AuthCookie is used to store user authentication
const AuthCookie string = "AuthCookie"

// DefaultAppURL .
const DefaultAppURL string = "http://localhost:1235/"

// TokenLifetime .
const TokenLifetime int = 60

// RenewalLifetime .
const RenewalLifetime int = 3600

// DefaultJwtKey .
const DefaultJwtKey string = "SecretKey"
