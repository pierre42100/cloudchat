module gitlab.com/pierre42100/cloudchat

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/satori/go.uuid v1.2.0
	go.mongodb.org/mongo-driver v1.7.3
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
