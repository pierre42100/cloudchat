package main

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/pierre42100/cloudchat/constants"
	"gitlab.com/pierre42100/cloudchat/utils"
)

func getRevocationRedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     utils.GetEnvString("REDIS_ADDR", "localhost:5268"),
		Password: utils.GetEnvString("REDIS_PASSWORD", ""),
		DB:       0,
	})
}

// Revoke a login token
func RevokeToken(token *utils.UserClaim) error {
	client := getRevocationRedisClient()
	ctx := context.Background()

	return client.Set(ctx, token.Uuid, "1", time.Second*time.Duration(constants.RenewalLifetime)*2).Err()
}

// IsRevoked is used to check whether a token has been revoked or not
func IsRevoked(token *utils.UserClaim) (bool, error) {
	client := getRevocationRedisClient()
	ctx := context.Background()

	_, err := client.Get(ctx, token.Uuid).Result()
	if err == redis.Nil {
		return false, nil
	} else if err != nil {
		return true, err
	}

	return true, nil
}
