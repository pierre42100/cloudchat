package main

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"strings"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/pierre42100/cloudchat/constants"
	"gitlab.com/pierre42100/cloudchat/mongousers"
	"gitlab.com/pierre42100/cloudchat/utils"
)

var (
	//go:embed static
	res embed.FS
)

func redirectCb(w http.ResponseWriter, cb string, jwt string) {
	cb = fmt.Sprintf("%s?token=%s", cb, jwt)

	utils.Redirect(w, cb)
}

func getAuthClaim(req *http.Request) *utils.UserClaim {
	claim := utils.GetAuthClaim(req, false, true)
	if claim == nil {
		return nil
	}

	// Check if token was revoked
	if revoked, _ := IsRevoked(claim); revoked {
		log.Printf("Got a revoked login token")
		return nil
	}

	return claim
}

func redirAuth(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Location", "/auth")
	w.WriteHeader(301)
}

func bootstrap(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	bytes, _ := res.ReadFile("static/bootstrap.css")
	w.Write(bytes)
}

func script(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/script.js")
	w.Write(bytes)
}

func signin(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	bytes, _ := res.ReadFile("static/signin.css")
	w.Write(bytes)
}

func buildPage(title string, cb string, tpl string, danger string, success string) string {
	bytes, _ := res.ReadFile("static/index.html")
	page := string(bytes)

	template, _ := (res.ReadFile(tpl))
	page = strings.Replace(page, "{body}", string(template), 1)

	page = strings.ReplaceAll(page, "{title}", title)
	page = strings.ReplaceAll(page, "{danger}", danger)
	page = strings.ReplaceAll(page, "{success}", success)
	page = strings.ReplaceAll(page, "{cb}", cb)

	return page
}

func finishLogin(w http.ResponseWriter, cb string, user *mongousers.User) {

	jwt, err := utils.GenerateJWTToken(uuid.NewV4().String(), user.UID)

	if err != nil {
		panic(err)
	}

	utils.SetCookieHeader(w, jwt)
	redirectCb(w, cb, jwt)
}

func auth(w http.ResponseWriter, req *http.Request) {
	mail := ""
	password := ""
	err := ""
	success := ""
	cb := utils.GetEnvString("DEFAULT_REDIRECT", constants.DefaultAppURL)

	if req.URL.Query().Has("cb") {
		cb = req.URL.Query().Get("cb")
	}

	if req.URL.Query().Has("err") {
		err = req.URL.Query().Get("err")
	}

	if req.URL.Query().Has("success") {
		success = req.URL.Query().Get("success")
	}

	// Check if user is already signed in
	login := getAuthClaim(req)
	if login != nil {
		token, err := utils.GenerateJWTToken(login.Uuid, login.UserID)
		if err != nil {
			panic(err)
		}

		utils.SetCookieHeader(w, token)
		redirectCb(w, cb, token)
		return
	}

	if req.Method == "POST" {
		req.ParseForm()
		mail = req.Form.Get("mail")
		password = req.Form.Get("password")

		if !utils.IsMailValid(mail) {
			err = "Email address is invalid"
		}

		if len(password) < constants.MinPasswordLength {
			err = "Password is too short!"
		}

		var user *mongousers.User = nil

		if err == "" {
			conn := mongousers.Connect()
			defer conn.Disconnect()

			var e error
			if user, e = conn.FindUserByEmail(mail); e != nil {
				panic(e)
			}

			if user == nil || !utils.CheckPasswordHash(password, user.PasswordHash) {
				err = "Email address or password is invalid!"
			}
		}

		if user != nil && err == "" {
			success = fmt.Sprintf("Welcome %s", user.Name)

			finishLogin(w, cb, user)
			return
		}
	}

	page := buildPage("Sign in", cb, "static/login.html", err, success)
	page = strings.ReplaceAll(page, "{mail}", mail)
	w.Write([]byte(page))
}

func signup(w http.ResponseWriter, req *http.Request) {
	cb := utils.GetEnvString("DEFAULT_REDIRECT", constants.DefaultAppURL)
	err := ""
	success := ""
	name := ""
	email := ""
	password := ""

	if req.URL.Query().Has("cb") {
		cb = req.URL.Query().Get("cb")
	}

	if req.URL.Query().Has("err") {
		err = req.URL.Query().Get("err")
	}

	if req.URL.Query().Has("success") {
		success = req.URL.Query().Get("success")
	}

	if req.Method == "POST" {
		req.ParseForm()
		name = req.Form.Get("name")
		email = req.Form.Get("email")
		password = req.Form.Get("password")

		if len(name) < constants.MinNameLength {
			err = "Name is invalid!"
		}

		if !utils.IsMailValid(email) {
			err = "Invalid email address"
		}

		if len(password) < constants.MinPasswordLength {
			err = "Password is too short!"
		}

		myuuid := uuid.NewV4().String()

		hash, _ := utils.HashPassword(password)

		user := mongousers.User{
			UID:          myuuid,
			Name:         name,
			Mail:         email,
			PasswordHash: hash,
		}

		if len(err) == 0 {
			conn := mongousers.Connect()
			defer conn.Disconnect()

			exists, e := conn.EmailExists(email)
			if e != nil {
				panic(e)
			}

			if exists {
				err = "An account using the same email address already exists!"
			} else {
				conn.InsertUser(&user)
				success = "Account created successfully"

				finishLogin(w, cb, &user)
				return
			}
		}
	}

	page := buildPage("Sign up", cb, "static/signup.html", err, success)
	page = strings.ReplaceAll(page, "{name}", name)
	page = strings.ReplaceAll(page, "{email}", email)
	page = strings.ReplaceAll(page, "{password}", "") //password)
	w.Write([]byte(page))
}

func newToken(w http.ResponseWriter, req *http.Request) {
	login := getAuthClaim(req)

	if login == nil {
		w.WriteHeader(401)
		return
	}

	jwt, err := utils.GenerateJWTToken(login.Uuid, login.UserID)

	if err != nil {
		panic(err)
	}

	utils.SetCookieHeader(w, jwt)
	w.Header().Add("Access-Control-Allow-Origin", req.Header.Get("Origin"))
	w.Header().Add("Access-Control-Allow-Credentials", "true")
	w.Write([]byte(jwt))
}

func signout(w http.ResponseWriter, req *http.Request) {
	login := getAuthClaim(req)

	if login == nil {
		utils.Redirect(w, "/auth?err=Not%20signed%20in")
		return
	}

	RevokeToken(login)
	utils.SetCookieHeader(w, "")
	utils.Redirect(w, "/auth?success=Goodbye%20!")
}

func main() {
	listenAddr := utils.GetEnvString("LISTEN_ADDR", "127.0.0.1:1234")

	fmt.Printf("Will listen on http://%s/\n", listenAddr)

	// Start server
	http.HandleFunc("/assets/bootstrap.css", bootstrap)
	http.HandleFunc("/assets/signin.css", signin)
	http.HandleFunc("/assets/script.js", script)
	http.HandleFunc("/", redirAuth)
	http.HandleFunc("/auth", auth)
	http.HandleFunc("/signup", signup)
	http.HandleFunc("/new_token", newToken)
	http.HandleFunc("/sign_out", signout)
	http.ListenAndServe(listenAddr, nil)
}
