let messages = [];

function peerID() {
    return location.hash.replace("#", "")
}

async function getMessagesList(peerID) {
    return await (await fetch("/api/messages/with_peer?peer=" + peerID)).json();
}

const outgoingmsgtpl = document.querySelector("#messages_target > div:first-child").outerHTML
const incomingmsgtpl = document.querySelector("#messages_target > div:last-child").outerHTML

async function renderMessagesList() {
    const nodes = [];

    for(let msg of messages) {
        let html = msg.UserID === user_id ? outgoingmsgtpl : incomingmsgtpl;
        html = html.replace("{message}", msg.Message)
            .replace("{time}", timeDiff(msg.Time))

        let user = await getUserInfo(msg.UserID);
        html = html.replace("{user}", user.Name);
        
        const node = document.createElement("div");
        node.innerHTML = html;
        nodes.push(node)
    }

    const target = byId("messages_target");
    target.innerHTML = "";
    nodes.forEach(n => target.appendChild(n))
    target.scrollTo({
        behavior: "smooth",
        top: target.scrollHeight,
    })
}

async function addMessage(msg) {
    if (msg.DestUserID!=peerID() && msg.UserID != peerID)
        return;

    messages.push(msg);
    renderMessagesList();
}

async function ChangeActivePeer() {
    document.querySelector(".right-pane").style.visibility = peerID() === "" ? "hidden": "visible";
    if (peerID() === "")
        return;

    const peerInfo = await getUserInfo(peerID());

    byId("peerName").innerHTML = peerInfo.Name;
    
    messages = await getMessagesList(peerID());
    renderMessagesList();
}

async function sendNewMessage() {
    try {
        const message = byId("messageInput").value;
        if(message === "")
            return false;
        
        await fetch("/api/messages/send_text", {
            method: "POST",
            body: new URLSearchParams({
                "peer_id": peerID(),
                "content": message
            }),
        })

        byId("messageInput").value = "";

    } catch(e) {
        console.error(e);
        alert("Failed to send message!");
    }
    return false;
}

window.addEventListener("hashchange", () => ChangeActivePeer());