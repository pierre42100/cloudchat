let user_cache = new Map();

/**
 * @param {String} id 
 * @returns {HTMLElement}
 */
function byId(id) {
    return document.getElementById(id)
}

/**
 * Get information about a user
 * 
 * @param {string} id 
 * @returns User information
 */
async function getUserInfo(id) {
    if(!user_cache.has(id))
        user_cache.set(id, await(await fetch("/api/user/info?id=" + id)).json())
    return user_cache.get(id)
}

function timeDiff(time) {
    const diff = Math.floor(new Date().getTime()/1000) - time;
    if (diff < 60)
        return diff + "s"
    
    if (diff < 3600)
        return Math.floor(diff/60) + "m"
    
    if (diff < 3600 * 24)
        return Math.floor(diff/3600) + "h"
    
        return Math.floor(diff/3600 * 24) + "d"
}

function MessagePeerID(message) {
    return message.UserID == user_id ? message.DestUserID : message.UserID;
}

function refreshActiveLinks() {
    document.querySelectorAll("a.active").forEach(el => el.classList.remove("active"))

    document.querySelectorAll("a").forEach(el => {
        if (el.href === location.href)
            el.classList.add("active")
    })
}

window.addEventListener("hashchange", () => refreshActiveLinks())