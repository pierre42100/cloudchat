let user_id = "";

// Auto-refresh token
async function refreshAuthToken() {
    try {
        const token = await (await fetch(env.auth_origin+"/new_token",{"credentials":"include"})).text()

        await fetch("/api/set_token", {
            method: "POST",
            body: new URLSearchParams({
                "token": token
            }),
        })
    } catch(e) {
        console.error(e)
        alert("Failed to refresh auth token!");
    }
}



async function init() {
    try {
        // Refresh login token
        await refreshAuthToken();
        setInterval(() => refreshAuthToken(), 25 *1000)

        // Get current user information
        user_id = (await(await fetch("/api/user_id")).json()).ID
        const userInfo = await getUserInfo(user_id);

        // Initialize header
        byId("username").innerHTML = userInfo.Name
        byId("accountLink").href = env.account_origin

        // Initialize left pane
        await refreshLatestMessages();

        // Initialize search
        byId("search_field").onkeyup = () => refreshSearch();

        await ChangeActivePeer();

        // Initialize send message form
        byId("sendMessageForm").onsubmit = () => sendNewMessage();

    } catch(e) {
        console.error(e)
    }
}

init();