let leftpanetpl = byId("latestMessages").children[0].innerHTML;
function refreshLeftPane(messages, users) {
    let nodes = []
    for (let message of messages) {
        const user = users.get(MessagePeerID(message));

        const a = document.createElement("a");
        a.href = "#" + MessagePeerID(message)
        a.className = "list-group-item list-group-item-action border-0";
        
        a.innerHTML = leftpanetpl
            .replace("{user}", user.Name)
            .replace("{lastmessage}", message.Message)
            .replace("{time}", message.Time ? timeDiff(message.Time) : "");
        nodes.push(a)
    }

    const target = byId("latestMessages");
    target.innerHTML = "";
    nodes.forEach(n => target.appendChild(n));

    refreshActiveLinks();
}

let last_messages_cache = new Map()

async function refreshLatestMessages() {
    const messages = await (await fetch("/api/messages/latest_for_threads")).json();
    const users = new Map();
    for(let message of messages) {
        users.set(message.UserID, await getUserInfo(message.UserID));
        users.set(message.DestUserID, await getUserInfo(message.DestUserID));

        last_messages_cache.set(MessagePeerID(message), message);
    }

    refreshLeftPane(messages, users);
}


async function refreshSearch() {
    const query = byId("search_field").value;

    if (query === "")
    {
        refreshLatestMessages()
        return;
    }

    const results = await (await fetch("/api/user/search?q=" + encodeURIComponent(query))).json()

    const messages = []
    const users = new Map()

    for(let result of results) {
        messages.push({
            UserID: user_id,
            DestUserID: result.UID,
            Message: last_messages_cache.has(result.UID) ? last_messages_cache.get(result.UID).Message : "",
        })

        users.set(result.UID, await getUserInfo(result.UID))
    }

    refreshLeftPane(messages, users)
}

window.addEventListener("hashchange", () => {
    byId("search_field").value = "";
    refreshSearch();
})