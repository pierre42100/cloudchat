package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pierre42100/cloudchat/mongousers"
	"gitlab.com/pierre42100/cloudchat/utils"
)

func settoken(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	utils.SetCookieHeader(w, req.Form.Get("token"))
}

type resUserID struct {
	ID string
}

func userid(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	bytes, _ := json.Marshal(resUserID{ID: claim.UserID})
	w.Header().Add("Content-Type", "application/json")
	w.Write(bytes)
}

type resUserInfo struct {
	ID   string
	Name string
}

func userinfo(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	conn := mongousers.Connect()
	defer conn.Disconnect()

	user, err := conn.FindUserByID(req.URL.Query().Get("id"))
	if err != nil {
		w.WriteHeader(500)
		return
	}

	if user == nil {
		w.WriteHeader(404)
		return
	}

	bytes, _ := json.Marshal(resUserInfo{ID: user.UID, Name: user.Name})
	w.Header().Add("Content-Type", "application/json")
	w.Write(bytes)
}

func searchuser(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	conn := mongousers.Connect()
	defer conn.Disconnect()

	results, err := conn.SearchUser(req.URL.Query().Get("q"))
	if err != nil {
		log.Printf("Failed to search users! %s", err)
		w.WriteHeader(500)
		return
	}

	bytes, _ := json.Marshal(results)
	w.Header().Add("Content-Type", "application/json")
	w.Write(bytes)
}
