package main

import (
	"fmt"
	"net/http"

	"gitlab.com/pierre42100/cloudchat/utils"
)

func checkAuthToken(w http.ResponseWriter, req *http.Request) *utils.UserClaim {
	claim := utils.GetAuthClaim(req, true, false)
	if claim == nil {
		utils.Redirect(w, fmt.Sprintf(
			"%s/auth?cb=%s/authcb",
			utils.GetEnvString("AUTH_URL", "http://localhost:1234"),
			utils.GetEnvString("CHAT_URL", "http://localhost:1235"),
		))
	}

	return claim
}
