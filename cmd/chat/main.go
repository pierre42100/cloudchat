package main

import (
	"fmt"
	"net/http"

	"gitlab.com/pierre42100/cloudchat/utils"
)

func main() {
	listenAddr := utils.GetEnvString("LISTEN_ADDR", "127.0.0.1:1235")

	fmt.Printf("Will listen on http://%s/\n", listenAddr)

	// Start server
	// Static files
	http.HandleFunc("/assets/bootstrap.css", bootstrapcss)
	http.HandleFunc("/assets/bootstrap.bundle.min.js", bootstrapjs)
	http.HandleFunc("/assets/account.svg", accountsvg)
	http.HandleFunc("/assets/chat.css", chatcss)
	http.HandleFunc("/assets/script.js", scriptjs)
	http.HandleFunc("/assets/utils.js", utilsjs)
	http.HandleFunc("/assets/leftpane.js", leftpanejs)
	http.HandleFunc("/assets/rightpane.js", rightpanejs)

	// API methods
	http.HandleFunc("/api/set_token", settoken)
	http.HandleFunc("/api/user_id", userid)
	http.HandleFunc("/api/user/info", userinfo)
	http.HandleFunc("/api/user/search", searchuser)
	http.HandleFunc("/api/messages/latest_for_threads", latestMessages)
	http.HandleFunc("/api/messages/with_peer", messagesWithPeer)
	http.HandleFunc("/api/messages/send_text", sendTextMessage)

	// "Full" pages
	http.HandleFunc("/authcb", authcb)
	http.HandleFunc("/signout", signout)
	http.HandleFunc("/", home)

	http.ListenAndServe(listenAddr, nil)
}
