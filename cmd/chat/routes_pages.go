package main

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/pierre42100/cloudchat/utils"
)

func home(w http.ResponseWriter, req *http.Request) {

	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	pageb, _ := res.ReadFile("static/index.html")
	page := (string)(pageb)

	page = strings.ReplaceAll(page, "{signout}", "/signout")
	page = strings.ReplaceAll(page, "{auth_origin}", utils.GetEnvString("AUTH_URL", "http://localhost:1234"))
	page = strings.ReplaceAll(page, "{account_origin}", utils.GetEnvString("ACCOUNT_URL", "http://localhost:1236"))

	w.Write(([]byte)(page))
}

func authcb(w http.ResponseWriter, req *http.Request) {
	utils.SetCookieHeader(w, req.URL.Query().Get("token"))
	utils.Redirect(w, "/")
}

func signout(w http.ResponseWriter, req *http.Request) {
	utils.SetCookieHeader(w, "")
	utils.Redirect(w, fmt.Sprintf("%s/sign_out", utils.GetEnvString("AUTH_URL", "http://localhost:1234")))
}
