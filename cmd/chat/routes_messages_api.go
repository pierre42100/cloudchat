package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pierre42100/cloudchat/mongousers"
)

type resMessage struct {
	ID         string
	UserID     string
	DestUserID string
	Message    string
	Time       uint64
}

func latestMessages(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	messages := []resMessage{
		{
			ID:         "1",
			UserID:     "f5cc08b1-7002-412a-9d35-3b0e3b19310e",
			DestUserID: "6d7f6606-6439-4acf-83b9-acdab78fde3e",
			Message:    "A message from toto for you",
			Time:       1638625968,
		},

		{
			ID:         "1",
			UserID:     "6d7f6606-6439-4acf-83b9-acdab78fde3e",
			DestUserID: "6a766e46-628d-40f5-b554-a4875cca3a26",
			Message:    "A message from you for tata",
			Time:       1638615968,
		},
	}

	bytes, _ := json.Marshal(messages)
	w.Header().Add("Content-Type", "application/json")
	w.Write(bytes)
}

func messagesWithPeer(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	peerID := req.URL.Query().Get("peer")
	userID := claim.UserID

	var time uint64 = 1638615968

	messages := []resMessage{
		{
			ID:         "1",
			UserID:     peerID,
			DestUserID: userID,
			Message:    "Hey you !!!",
			Time:       (time + 1),
		},

		{
			ID:         "2",
			UserID:     userID,
			DestUserID: peerID,
			Message:    "Hi ! How do you do ?",
			Time:       (time + 60),
		},

		{
			ID:         "3",
			UserID:     userID,
			DestUserID: peerID,
			Message:    "It has been a while !",
			Time:       (time + 70),
		},

		{
			ID:         "4",
			UserID:     peerID,
			DestUserID: userID,
			Message:    "Yes !!!",
			Time:       (time + 120),
		},
	}

	bytes, _ := json.Marshal(messages)
	w.Header().Add("Content-Type", "application/json")
	w.Write(bytes)
}

func sendTextMessage(w http.ResponseWriter, req *http.Request) {
	claim := checkAuthToken(w, req)
	if claim == nil {
		return
	}

	req.ParseForm()
	peerID := req.Form.Get("peer_id")
	content := req.Form.Get("content")

	usersConn := mongousers.Connect()
	defer usersConn.Disconnect()

	user, err := usersConn.FindUserByID(peerID)
	if err != nil {
		log.Printf("Could not send message : %s", err)
		w.WriteHeader(500)
		return
	}

	if user == nil {
		log.Printf("Could not send message : user not found!")
		w.WriteHeader(404)
		return
	}

	// TODO : replace with real method call
	log.Printf("Send a message to %s with content %s", user.Name, content)
}
