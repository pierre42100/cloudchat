package main

import (
	"embed"
	"net/http"
)

var (
	//go:embed static
	res embed.FS
)

func bootstrapcss(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	bytes, _ := res.ReadFile("static/assets/bootstrap.css")
	w.Write(bytes)
}

func bootstrapjs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/bootstrap.bundle.min.js")
	w.Write(bytes)
}

func accountsvg(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "image/svg+xml")
	bytes, _ := res.ReadFile("static/assets/account.svg")
	w.Write(bytes)
}

func chatcss(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	bytes, _ := res.ReadFile("static/assets/chat.css")
	w.Write(bytes)
}

func scriptjs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/script.js")
	w.Write(bytes)
}

func utilsjs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/utils.js")
	w.Write(bytes)
}

func leftpanejs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/leftpane.js")
	w.Write(bytes)
}

func rightpanejs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/rightpane.js")
	w.Write(bytes)
}
