// Remove un-used alerts
document.querySelectorAll("[role=alert]").forEach(el => {
    if(el.innerHTML.trim() === "")
        el.remove();
})