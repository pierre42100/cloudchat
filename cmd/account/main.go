package main

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.com/pierre42100/cloudchat/constants"
	"gitlab.com/pierre42100/cloudchat/mongousers"
	"gitlab.com/pierre42100/cloudchat/utils"
)

var (
	//go:embed static
	res embed.FS
)

func bootstrapcss(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	bytes, _ := res.ReadFile("static/assets/bootstrap.css")
	w.Write(bytes)
}

func bootstrapjs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/bootstrap.bundle.min.js")
	w.Write(bytes)
}

func scriptjs(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Content-Type", "application/javascript")
	bytes, _ := res.ReadFile("static/assets/script.js")
	w.Write(bytes)
}

func home(w http.ResponseWriter, req *http.Request) {
	danger := ""
	success := ""

	claim := utils.GetAuthClaim(req, true, false)
	if claim == nil {
		utils.Redirect(w, fmt.Sprintf(
			"%s/auth?cb=%s/authcb",
			utils.GetEnvString("AUTH_URL", "http://localhost:1234"),
			utils.GetEnvString("ACCOUNT_URL", "http://localhost:1235"),
		))
		return
	}

	pageb, _ := res.ReadFile("static/index.html")
	page := (string)(pageb)

	conn := mongousers.Connect()
	defer conn.Disconnect()

	user, err := conn.FindUserByID(claim.UserID)
	if err != nil {
		log.Printf("Failed to get user information! %s", err)
		page = strings.ReplaceAll(page, "{danger}", "Failed to get user information!")
		w.Write([]byte(page))
		return
	}

	// Check if user information have to be updated
	if req.Method == "POST" {
		req.ParseForm()

		user.Name = req.Form.Get("name")
		user.Mail = req.Form.Get("email")

		currPassword := req.Form.Get("currPassword")
		newPassword := req.Form.Get("newPassword")
		newPasswordConfirmation := req.Form.Get("confirmNewPassword")

		if !utils.IsMailValid(user.Mail) {
			danger = "Specified email address is invalid!"
		} else if len(user.Name) < constants.MinNameLength {
			danger = "Your name is too short!"
		} else if newPassword != "" && currPassword == "" {
			danger = "You must input your current password to change it"
		} else if currPassword != "" && newPassword == "" {
			danger = "Please specify your current password only to change it!"
		} else if newPassword != newPasswordConfirmation {
			danger = "New password and its confirmation are different"
		} else if newPassword != "" && len(newPassword) < constants.MinPasswordLength {
			danger = "Your new password is too short!"
		} else if currPassword != "" && !utils.CheckPasswordHash(currPassword, user.PasswordHash) {
			danger = "Your old password was not recognized!"
		}

		if newPassword != "" {
			hash, err := utils.HashPassword(newPassword)

			if err != nil {
				danger = "Failed to hash password!"
				log.Printf("Failed to hash new password! %s", err)
			}

			user.PasswordHash = hash
		}

		if danger == "" {
			if err := conn.UpdateUser(user); err != nil {
				danger = "Failed to update user information!"
				log.Printf("Failed to update user account! %s", err)
			} else {
				success = "Your account was successfully updated!"
			}
		}

	}

	page = strings.ReplaceAll(page, "{name}", user.Name)
	page = strings.ReplaceAll(page, "{mail}", user.Mail)
	page = strings.ReplaceAll(page, "{danger}", danger)
	page = strings.ReplaceAll(page, "{success}", success)
	page = strings.ReplaceAll(page, "{signout}", "/signout")

	w.Write(([]byte)(page))
}

func authcb(w http.ResponseWriter, req *http.Request) {
	utils.SetCookieHeader(w, req.URL.Query().Get("token"))
	utils.Redirect(w, "/")
}

func signout(w http.ResponseWriter, req *http.Request) {
	utils.SetCookieHeader(w, "")
	utils.Redirect(w, fmt.Sprintf("%s/sign_out", utils.GetEnvString("AUTH_URL", "http://localhost:1234")))
}

func main() {
	listenAddr := utils.GetEnvString("LISTEN_ADDR", "127.0.0.1:1236")

	fmt.Printf("Will listen on http://%s/\n", listenAddr)

	// Start server
	http.HandleFunc("/assets/bootstrap.css", bootstrapcss)
	http.HandleFunc("/assets/bootstrap.bundle.min.js", bootstrapjs)
	http.HandleFunc("/assets/script.js", scriptjs)
	http.HandleFunc("/authcb", authcb)
	http.HandleFunc("/signout", signout)
	http.HandleFunc("/", home)

	http.ListenAndServe(listenAddr, nil)
}
