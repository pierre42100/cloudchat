#!/bin/bash
mkdir -p storage/users_mongo storage/users_redis
docker run --rm -v $(pwd)/storage/users_mongo:/data/db -p 5267:27017 mongo:5 &
docker run --rm -v $(pwd)/storage/users_redis:/data -p 5268:6379 redis:6