package mongousers

import (
	"context"
	"log"
	"strings"
	"time"

	"gitlab.com/pierre42100/cloudchat/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const database string = "users"
const collection string = "users"

// User contains all information about a single user
type User struct {
	UID          string
	Name         string
	Mail         string
	PasswordHash string
}

// Conn handles a connection to a Mongo database.
//
// There should be only one connection to Mongo database
// for the entire application
type Conn struct {
	client *mongo.Client
}

// Connect to database
func Connect() Conn {
	uri := utils.GetEnvString("MONGO_ADDR", "mongodb://localhost:5267")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatalf("Could not connect to Users mongo database ! %v", err)
	}

	return Conn{
		client: client,
	}
}

// Disconnect from database
func (c Conn) Disconnect() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := c.client.Disconnect(ctx); err != nil {
		panic(err)
	}
}

func (c *Conn) collection() *mongo.Collection {
	return c.client.Database(database).Collection(collection)
}

// FindUserByID .
func (c Conn) FindUserByID(id string) (*User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := c.collection().Find(ctx, bson.D{primitive.E{Key: "uid", Value: id}})

	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	var users []User
	if err = cur.All(ctx, &users); err != nil {
		return nil, err
	}

	if len(users) == 0 {
		return nil, nil
	}

	return &users[0], nil
}

// FindUserByEmail .
func (c Conn) FindUserByEmail(email string) (*User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := c.collection().Find(ctx, bson.D{primitive.E{Key: "mail", Value: email}})

	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	var users []User
	if err = cur.All(ctx, &users); err != nil {
		return nil, err
	}

	if len(users) == 0 {
		return nil, nil
	}

	return &users[0], nil
}

// EmailExists .
func (c Conn) EmailExists(mail string) (bool, error) {
	user, err := c.FindUserByEmail(mail)
	if err != nil {
		return false, err
	}

	return user != nil, nil
}

// InsertUser inserts a new user into the database
func (c Conn) InsertUser(user *User) error {
	b, err := bson.Marshal(user)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = c.collection().InsertOne(ctx, b)

	return err
}

// UpdateUser update user information
func (c Conn) UpdateUser(user *User) error {
	b, err := bson.Marshal(user)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = c.collection().ReplaceOne(ctx, bson.D{primitive.E{Key: "uid", Value: user.UID}}, b)

	return err
}

// SearchUser search for user in the database
func (c Conn) SearchUser(name string) ([]User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := c.collection().Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	users := []User{}

	for cur.Next(ctx) {
		var user User
		if err := cur.Decode(&user); err != nil {
			return nil, err
		}

		if strings.Contains(strings.ToLower(user.Name), strings.ToLower(name)) {
			users = append(users, user)

			if len(users) > 50 {
				break
			}
		}
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return users, nil
}
