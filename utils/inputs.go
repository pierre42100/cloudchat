package utils

import "net/mail"

// IsMailValid check out wheter a given email address
// is valid or not
func IsMailValid(m string) bool {
	_, err := mail.ParseAddress(m)
	return err == nil
}