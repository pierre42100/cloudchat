package utils

import (
	"fmt"
	"os"
)

// GetEnvString retrieve the value of an environment
// variable. If it does not exist, a warning is printed,
// alongside with the default value used as replacment.
func GetEnvString(v string, fallback string) string {
	val := os.Getenv(v)

	if len(val) == 0 {
		val = fallback

		fmt.Fprintf(os.Stderr, "%s env variable is missing, using %s as fallback\n", v, fallback)
	}

	return val
}
