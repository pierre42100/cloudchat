package utils

import "net/http"

// Redirect user on another page
func Redirect(w http.ResponseWriter, url string) {
	w.Header().Add("Location", url)
	w.WriteHeader(302)
}
