package utils

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/golang-jwt/jwt"
	"gitlab.com/pierre42100/cloudchat/constants"
)

// UserClaim .
type UserClaim struct {
	Uuid       string
	UserID     string
	ExpTime    int64
	RefreshExp int64
}

// Valid .
func (t UserClaim) Valid(CheckExp bool, CheckRenewal bool) error {
	if CheckExp && t.ExpTime < Time() {
		return fmt.Errorf("Token is expirdd")
	}

	if CheckRenewal && t.RefreshExp < Time() {
		return fmt.Errorf("Renewal is expired")
	}

	return nil
}

// GenerateJWTToken generates a new JWT token
func GenerateJWTToken(uuid string, userID string) (string, error) {
	claim := UserClaim{
		Uuid:       uuid,
		UserID:     userID,
		ExpTime:    (int64)(constants.TokenLifetime) + Time(),
		RefreshExp: (int64)(constants.RenewalLifetime) + Time(),
	}

	js, _ := json.Marshal(claim)

	c := jwt.MapClaims{}
	c["content"] = (string)(js)

	token := jwt.NewWithClaims(
		jwt.SigningMethodHS512,
		c,
	)

	return token.SignedString([]byte(GetEnvString("JWT_KEY", constants.DefaultJwtKey)))
}

// DecodeJWTToken decodes & validate a token
func DecodeJWTToken(str string) (*UserClaim, error) {
	token, err := jwt.Parse(str, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(GetEnvString("JWT_KEY", constants.DefaultJwtKey)), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok {
		return nil, fmt.Errorf("Failed to get token")
	}

	if err := claims.Valid(); err != nil {
		return nil, err
	}

	jsonstring, ok := (claims["content"]).(string)
	if !ok {
		return nil, fmt.Errorf("failed to extract content")
	}

	c := new(UserClaim)
	if err := json.Unmarshal(([]byte)(jsonstring), c); err != nil {
		return nil, err
	}

	return c, nil
}

// GetAuthClaim retrieves an auth claim from an HTTP request
func GetAuthClaim(req *http.Request, CheckExp bool, CheckRenewal bool) *UserClaim {
	cookie, err := req.Cookie(constants.AuthCookie)
	if err != nil {
		return nil
	}

	token, err := DecodeJWTToken(cookie.Value)
	if err != nil {
		log.Printf("Failed to decode JWT token: %s", err)
		return nil
	}

	if err := token.Valid(CheckExp, CheckRenewal); err != nil {
		log.Printf("Got an invalid JWT token! %s", err)
		return nil
	}

	return token
}

// SetCookieHeader sets new cookie
func SetCookieHeader(w http.ResponseWriter, jwt string) {
	w.Header().Add("Set-Cookie", fmt.Sprintf("%s=\"%s\"; Same-Site=None; Secure", constants.AuthCookie, jwt))
}
