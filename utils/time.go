package utils

import "time"

// Time returns the number of seconds since epoch
func Time() int64 {
	return time.Now().Unix();
}
